package com.luwakdev.msp.UI.Scan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.zxing.Result;
import com.luwakdev.msp.R;
import com.luwakdev.msp.UI.List.ListActivity;

import java.io.IOException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static java.lang.System.exit;

/**
 * Created by krisnasatria on 3/18/18.
 */

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    private ProgressDialog pDialog;
    private Intent i;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // If you would like to resume scanning, call this method below:
        if (rawResult.getText() != null) {
            mScannerView.stopCamera();
            url = rawResult.getText();
            doGetList(url);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScannerView.resumeCameraPreview(ScanActivity.this);
                }
            }, 2000);
        } else {
            mScannerView.resumeCameraPreview(this);
        }
    }

    private void doGetList(String url) {
        i = new Intent(ScanActivity.this, ListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        i.putExtras(bundle);
        startActivity(i);
        finish();
    }
}

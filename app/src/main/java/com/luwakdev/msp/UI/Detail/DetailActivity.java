package com.luwakdev.msp.UI.Detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.jsibbold.zoomage.ZoomageView;
import com.luwakdev.msp.R;
import com.luwakdev.msp.Utils.ImageLoader;
import com.robertsimoes.shareable.Shareable;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by krisnasatria on 3/18/18.
 */

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.myZoomageView)
    ZoomageView zoomageView;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;

    String images = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        images = getIntent().getExtras().getString("images");

        Glide.with(this)
                .load(images)
                .into(zoomageView);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my image at : "+images);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
    }

}

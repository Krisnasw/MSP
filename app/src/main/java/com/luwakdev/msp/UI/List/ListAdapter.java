package com.luwakdev.msp.UI.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.luwakdev.msp.Data.ListImage;
import com.luwakdev.msp.R;
import com.luwakdev.msp.UI.Detail.DetailActivity;
import com.luwakdev.msp.Utils.ImageLoader;

import java.util.List;

/**
 * Created by krisnasatria on 3/18/18.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    private Context context;
    private List<ListImage> cartList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
//        public TextView name, description, price, chef, timestamp;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
//            name = view.findViewById(R.id.name);
//            chef = view.findViewById(R.id.chef);
//            description = view.findViewById(R.id.description);
//            price = view.findViewById(R.id.price);
            thumbnail = view.findViewById(R.id.thumbnail);
//            timestamp = view.findViewById(R.id.timestamp);
        }
    }


    public ListAdapter(Context context, List<ListImage> cartList) {
        this.context = context;
        this.cartList = cartList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ListImage recipe = cartList.get(position);

        Glide.with(context)
                .load(recipe.getThumbnail())
                .into(holder.thumbnail);
//        ImageLoader imageLoader = new ImageLoader(context);
//        imageLoader.DisplayImage(recipe.getThumbnail(), holder.thumbnail);

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("images", recipe.getThumbnail());
                i.putExtras(bundle);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

}

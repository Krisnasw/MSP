package com.luwakdev.msp.Utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.luwakdev.msp.R;

/**
 * Created by krisnasatria on 3/17/18.
 */

public class AppUtils {

    private AppUtils() {

    }

    public static void openPlayStoreForApp(Context context) {
        final String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(context.getResources().getString(R.string.app_name) +appPackageName)));
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(context.getResources().getString(R.string.app_name) +appPackageName)));
        }
    }

}
